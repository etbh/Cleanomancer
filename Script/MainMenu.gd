extends Node2D


func _ready():
	$Main/Start.connect("pressed", self, "start_game")
	$Main/Credits.connect("pressed", self, "show_credits")
	$Main/Exit.connect("pressed", self, "quit_game")
	$Credits/Menu.connect("pressed", self, "show_main")
	
	if OS.get_name() == "HTML5":
		$Main.remove_child($Main/Exit)

func start_game():
	get_tree().change_scene("res://Scenes/Game.tscn")

func quit_game():
	get_tree().quit()

func show_credits():
	$Credits.visible = true
	$Main.visible = false

func show_main():
	$Credits.visible = false
	$Main.visible = true
