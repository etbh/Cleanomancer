tool
extends Sprite


export var turbo := false setget turbo_set

export (Texture) var keyboard
export (Texture) var keyboard_turbo
export (Texture) var gamepad
export (Texture) var gamepad_turbo

var gamepad_detected := false

func turbo_set(_turbo : bool) -> void:
	turbo = _turbo
	apply()

func apply():
	if gamepad_detected:
		if turbo:
			texture = gamepad_turbo
		else:
			texture = gamepad
	else:
		if turbo:
			texture = keyboard_turbo
		else:
			texture = keyboard

func _ready():
	check_gamepad()
	apply()
	Input.connect("joy_connection_changed", self, "check_gamepad")

func check_gamepad(var device = 0, var plugged = false):
	if not Engine.editor_hint:
		gamepad_detected = not Input.get_connected_joypads().empty()
		apply()
