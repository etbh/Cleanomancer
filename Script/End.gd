extends Node2D



func _ready():
	var file = File.new()
	file.open("user://finished.dat", File.WRITE)
	file.close()
	$Credits.check(true)
	$Credits/Menu.connect("pressed", self, "back_menu")
	
func back_menu():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
