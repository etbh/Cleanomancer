extends Node2D

export (Texture) var finished
export (Texture) var not_finished

func _ready():
	check()
	
func check(force := false):
	if File.new().file_exists("user://finished.dat") or force:
		$Background.texture = finished
	else:
		$Background.texture = not_finished
