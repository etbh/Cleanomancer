extends Node2D

class_name Game

func _ready():
	new_level($Level)
		
	

func new_level(var level :Node):
	$MusicManager.new_sounds(level.get_node("Floor/Drum").stream, level.get_node("Floor/Bass").stream, level.get_node("Floor/Lead").stream)
