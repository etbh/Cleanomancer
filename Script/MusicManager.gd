extends Node


onready var players = [
	$Drum as AudioStreamPlayer,
	$Bass as AudioStreamPlayer,
	$Lead as AudioStreamPlayer
]

#var swapping = {
#	players = [],
#	streams = []
#}

func new_sounds(drum, bass, lead):
	var sounds = [drum, bass, lead]
	for i in 3:
		if players[i].stream != sounds[i]:
			var position = players[i].get_playback_position()
			players[i].stream = sounds[i]
			players[i].play(position)
		if not players[i].playing:
			players[i].play()

#func _process(delta):
#
#	for i in swapping.players.size():
#		var player := swapping.players[i] as AudioStreamPlayer
#		var stream := swapping.streams[i] as AudioStream
#
#		player.volume_db = player.volume_db - delta * 10
